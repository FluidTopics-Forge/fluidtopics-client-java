/*
 * ##########################################################################
 * #                                                                        #
 * #  Copyright (C) 1999-2017 Antidot                                       #
 * #                                                                        #
 * #  Should you receive a copy of this source code, you must check you     #
 * #  have a proper, written authorization of Antidot to hold it. If you    #
 * #  don't have such an authorization, you must DELETE all source code     #
 * #  files in your possession, and inform Antidot of the fact you obtain   #
 * #  these files. Should you not comply to these terms, you can be         #
 * #  prosecuted in the extent permitted by applicable law.                 #
 * #                                                                        #
 * ##########################################################################
 */

package fluidtopics.fluidtopics_client_java;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;

public class FluidTopicsSender {
	private String Url;
	private String User;
	private File Fs;
	private String Pwd;
	private Object Auth;
	private String Src;

	public FluidTopicsSender(String url, String user, String password,
			String fpath, String source) {
		this.Url = url;
		this.User = user;
		this.Pwd = password;
		this.Fs = new File(fpath);
		this.Auth = this.ConvertToBase64(this.User, this.Pwd);
		this.Src = source;
	}

	public FluidTopicsSender(String url, String auth_64, String fpath,
			String source) {
		this.Url = url;
		this.User = null;
		this.Pwd = null;
		this.Fs = new File(fpath);
		this.Auth = auth_64;
		this.Src = source;
	}

	public FluidTopicsSender(String url, String user, String password, File fs,
			String source) {
		this.Url = url;
		this.User = user;
		this.Pwd = password;
		this.Fs = fs;
		this.Auth = this.ConvertToBase64(this.User, this.Pwd);
		this.Src = source;
	}

	public FluidTopicsSender(String url, String auth_64, File fs, String source) {
		this.Url = url;
		this.User = null;
		this.Pwd = null;
		this.Fs = fs;
		this.Auth = auth_64;
		this.Src = source;
	}

	private String ConvertToBase64(String user, String password) {
		String auth = user + ":" + password;
		byte[] message = auth.getBytes(StandardCharsets.UTF_8);
		return Base64.getEncoder().encodeToString(message);
	}

	private HttpEntity BuildMultiPart() {
		MultipartEntityBuilder form = MultipartEntityBuilder.create();

		form.addBinaryBody("bin", this.Fs);
		return form.build();
	}

	private String get_ws() {
		String url = this.Url;
		if (!url.endsWith("/")) {
			url += "/";
		}
		return url + "api/admin/khub/sources/" + this.Src + "/upload";
	}

	public URLConnection SendToMyInstance() throws MalformedURLException,
			IOException {
		URLConnection httpClient = new URL(this.get_ws()).openConnection();
		HttpEntity mb = BuildMultiPart();

		httpClient.setDoOutput(true);
		httpClient.addRequestProperty(mb.getContentType().getName(), mb
				.getContentType().getValue());
		httpClient.addRequestProperty("Authorization", "Basic " + this.Auth);
		httpClient.addRequestProperty("Content-Length",
				String.valueOf(mb.getContentLength()));

		OutputStream http_out_stream = httpClient.getOutputStream();
		mb.writeTo(http_out_stream);
		http_out_stream.close();
		httpClient.getInputStream().close();

		return httpClient;
	}

}
