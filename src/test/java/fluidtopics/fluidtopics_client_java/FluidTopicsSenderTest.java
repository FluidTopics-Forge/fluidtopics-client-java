package fluidtopics.fluidtopics_client_java;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import org.junit.Test;

public class FluidTopicsSenderTest {

	@Test
	public void uploadFpathTest() {
		// User account
		String user = "<account>";
		// PWD of your account
		String pwd = "<pwd>";
		// URL of your portal
		String url = "<yourPortal>";
		// Path of your zip file
		String fpath = "./testResources/Dita02.zip";
		// Source ID for FT
		String source = "dita";

		FluidTopicsSender ftc = new FluidTopicsSender(url, user, pwd, fpath,
				source);
		try {
			ftc.SendToMyInstance();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void uploadFileTest() {
		// User account
		String user = "<account>";
		// PWD of your account
		String pwd = "<pwd>";
		// URL of your portal
		String url = "<yourPortal>";
		// Path of your zip file
		String fpath = "./testResources/Dita02.zip";
		File fs = new File(fpath);
		// Source ID for FT
		String source = "dita";

		FluidTopicsSender ftc = new FluidTopicsSender(url, user, pwd, fs,
				source);
		try {
			ftc.SendToMyInstance();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
